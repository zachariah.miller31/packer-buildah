
# Download Go from google
wget https://dl.google.com/go/go1.13.4.linux-${ARCH}.tar.gz
 
# Unpack go
tar xvf go1.13.4.linux-${ARCH}.tar.gz
 
# Change the owner
chown -R root:root ./go
 
# move it
mv go /usr/local
 
# Cleanup your workspace and delete the archive
rm go1.13.4.linux-${ARCH}.tar.gz
 
# Add it to the PATH

printf '#!/bin/bash \nexport GOPATH=$HOME/work \nexport PATH=$PATH:/usr/local/go/bin:$GOPATH/bin \n' > /etc/profile.d/go.sh
 
# Logout by pressing STRG+D
source /etc/profile.d/go.sh
 
# Check if Go works
go version

# Install dependencies for ARM builder

 
# Clone repository
git clone https://github.com/mkaczanowski/packer-builder-arm
 
# Checkout the commit that worked for me
cd packer-builder-arm
#git checkout b1c268e9d33105634c899e9095f0af097e1af432
 
# Build the Packer ARM builder
go mod download
# go build -ldflags "-linkmode external -extldflags -static"
go build

# Download packer
wget https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_${ARCH}.zip
unzip packer_${PACKER_VERSION}_linux_${ARCH}.zip


# Compress
upx-ucl ./packer-builder-arm ./packer -1

 
