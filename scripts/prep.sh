echo 'DPkg::Post-Invoke {"/bin/rm -f /var/cache/apt/archives/*.deb || true";};' | tee /etc/apt/apt.conf.d/clean
apt-get update -qq 
apt-get install -qqy --no-install-recommends qemu-user-static qemu-utils ca-certificates dosfstools e2fsprogs gdisk kpartx parted libarchive-tools sudo xz-utils psmisc wget
rm -rf /var/lib/apt/lists/*
wget https://raw.githubusercontent.com/qemu/qemu/master/scripts/qemu-binfmt-conf.sh
wget https://raw.githubusercontent.com/multiarch/qemu-user-static/master/containers/latest/register.sh
mv register.sh register
chmod +x qemu-binfmt-conf.sh
chmod +x register
