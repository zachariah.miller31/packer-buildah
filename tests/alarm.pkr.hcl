locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }


source "arm" "alarm-rpi" {
  file_checksum_type    = "md5"
  file_checksum_url     = "http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-aarch64-latest.tar.gz.md5"
  file_target_extension = "tar.gz"
  file_unarchive_cmd    = ["bsdtar", "-xpf", "$ARCHIVE_PATH", "-C", "$MOUNTPOINT"]
  file_urls             = ["http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-aarch64-latest.tar.gz"]
  image_build_method    = "new"
  image_partitions {
    filesystem   = "vfat"
    mountpoint   = "/boot"
    name         = "boot"
    size         = "256M"
    start_sector = "2048"
    type         = "c"
  }
  image_partitions {
    filesystem   = "ext4"
    mountpoint   = "/"
    name         = "root"
    size         = "0"
    start_sector = "526336"
    type         = "83"
  }
  image_path                   = "ArchLinuxARM-rpi-aarch64.img"
  image_size                   = "2G"
  image_type                   = "dos"
  qemu_binary_destination_path = "/usr/bin/qemu-aarch64-static"
  qemu_binary_source_path      = "/usr/bin/qemu-aarch64-static"
}

build {
  sources = ["source.arm.alarm-rpi"]

  provisioner "shell" {
    inline = ["pacman-key --init", "pacman-key --populate archlinuxarm", "mv /etc/resolv.conf /etc/resolv.conf.bk", "echo 'nameserver 8.8.8.8' > /etc/resolv.conf", "pacman -Sy --noconfirm --needed", "pacman -S parted --noconfirm --needed", "sed -i 's/mmcblk0/mmcblk1/g' /etc/fstab"]
  }
  provisioner "file" {
    destination = "/tmp"
    source      = "tests/scripts/resizerootfs"
  }
  provisioner "shell" {
    script = "tests/scripts/bootstrap_resizerootfs.sh"
  }
//   "post-processors": [
//  {
//      "type": "flasher",
//      "device": "/dev/sdX",
//      "block_size": "4096",
//      "interactive": true
//  }
// ] 
}
  
